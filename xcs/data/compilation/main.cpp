// A few commonly used libraries
#include <iostream>
#include <cassert>
#include <vector>
#include <set>
#include <unordered_set>
#include <cmath>  // For dec_to_base
#include <limits> //       "
#include <string_view> // For type_name

namespace xcs {
  std::string dec_to_base(int n, std::size_t base = 10) {
    const std::string digits = "0123456789abcdef";
    std::string result = "";

    if (base < 2 || base > 16)
      throw std::invalid_argument("invalid base");

    int sign;
    if ((sign = n) < 0) // Record sign
      n = -n; // Make n positive

    do {
      result = digits[n % base] + result; // Get next digit
    } while ((n /= base) > 0); // Reduce number to convert

    if (sign < 0)
      result = "-" + result;

    return result;
  }

  std::string dec_to_base(double d, std::size_t base = 10) {
    // d must be in integer range, so that we can use the int-version of dec_to_base
    // to convert d's integral part
    assert(std::numeric_limits<int>::lowest() < d);
    assert(d < std::numeric_limits<int>::max());

    double d_integral, d_fractional;
    d_fractional = std::modf(d, &d_integral);
    // d == d_integral + d_fractional (modulo rounding)

    assert(0 <= d_fractional);
    assert(d_fractional < 1);

    auto result = dec_to_base((int) d_integral, base);
    result.push_back('.');

    // Abort after max_counter rounds, to avoid nontermination on decimal fractional numbers
    // that don't have a finite binary expansion
    const int max_counter = 16;
    unsigned int counter = 0;

    // The dec-to-bin algorithm from the lecture
    do {
      d_fractional *= 2;

      unsigned int b = d_fractional;
      result.push_back(48 + b); // Convert int 0/1 to char '0'/'1'

      d_fractional -= b;
      counter++;
    } while (d_fractional != 0 && counter < max_counter);

    assert(counter < max_counter);

    return result;
  }
}

/* Code to obtain type information taken from https://stackoverflow.com/a/58331141/491216 */
namespace xcs {
 template <typename T>
  constexpr std::string_view wrapped_type_name () {
    #ifdef __clang__
        return __PRETTY_FUNCTION__;
    #elif defined(__GNUC__)
        return  __PRETTY_FUNCTION__;
    #elif defined(_MSC_VER)
        return  __FUNCSIG__;
    #endif
  }

  class probe_type;
  constexpr std::string_view probe_type_name ("xcs::probe_type");
  constexpr std::string_view probe_type_name_elaborated ("class xcs::probe_type");
  constexpr std::string_view probe_type_name_used (wrapped_type_name<probe_type> ().find (probe_type_name_elaborated) != -1 ? probe_type_name_elaborated : probe_type_name);

  constexpr size_t prefix_size () {
    return wrapped_type_name<probe_type> ().find (probe_type_name_used);
  }

  constexpr size_t suffix_size () {
    return wrapped_type_name<probe_type> ().length () - prefix_size () - probe_type_name_used.length ();
  }

  template <typename T>
  std::string_view type_name () {
    constexpr auto type_name = wrapped_type_name<T> ();

    return type_name.substr(prefix_size (), type_name.length () - prefix_size () - suffix_size ());
  }
}

/* https://stackoverflow.com/questions/36296425 */
namespace xcs {
  template <typename T>
  constexpr bool is_lvalue(T&&) {
    return std::is_lvalue_reference<T>{};
  }
}

#define XCS_INSPECT(x) { \
  std::cout << \
    std::boolalpha << \
    "Expression: " << #x << "\n" << \
    "    type:   " << xcs::type_name<decltype(x)>() << "\n" << \
    "    lvalue: " << xcs::is_lvalue(x) << "\n" << \
    "    value:  " << (x) << "\n"; \
}

namespace xcs {
  // // Taken from https://en.cppreference.com/w/cpp/string/basic_string/replace#Example
  // std::size_t replace_all_substrings(std::string& where,
  //                                    const std::string& what,
  //                                    std::string with) {
  //   std::size_t count{};
  //   for (std::string::size_type pos{};
  //        where.npos != (pos = where.find(what.data(), pos, what.length()));
  //        pos += with.length(), ++count) {

  //     where.replace(pos, what.length(), with.data(), with.length());
  //     if (count >= 5) break;
  //   }

  //   return count;
  // }

  bool get_bit(unsigned value, unsigned position) {
    return (value >> position) & 1u;
  }

  const unsigned tt_max_variable_count = 6; // 2^6 = 64 = sizeof(uint64_t) = the max. height of the created truth table

  void set_bit(uint64_t& value, unsigned position) {
    value |= UINT64_C(1) << position;
  }

  template<typename F>
  uint64_t truth_table(unsigned variable_count,
                       std::string textual_representation,
                       F boolean_function,
                       bool verbose_output) {

    using namespace std::string_literals;
    assert(1 <= variable_count);
    assert(variable_count <= tt_max_variable_count); // truth table height is 2^variable_count

    for (unsigned i = 0; i < variable_count; ++i) {
      char ch = 'a' + i;

      // // Inside textial_representation, replace "v[0]" with "a", "v[1]" with "b",
      // // etc.
      // std::string what = "v["s + char('0' + i) + "]"s;
      // std::string with = std::string(1, ch);
      // replace_all_substrings(textual_representation, what, with);

      if (verbose_output) std::cout << ch << ' ';
    }

    if (verbose_output) {
      std::cout << " |  " << textual_representation << '\n';
      std::cout << std::string(variable_count * 2 + textual_representation.size() + 5, '-') << '\n';
    } else {
      std::cout << textual_representation << " ";
    }

    // More inputs than necessary (i.e. more than variable_count), so that defining all
    // the aliases a, b, c, etc. in the macro below is defined behaviour.
    std::vector<bool> inputs(tt_max_variable_count, false);

    uint64_t result = 0;

    for (unsigned p = 0 ; p < std::pow(2, variable_count) ; ++p ) {
      for (unsigned i = 0; i < variable_count; ++i) {
        inputs[i] = get_bit(p, i);

        if (verbose_output) std::cout << inputs[i] << ' ';
      }

      bool r = boolean_function(inputs);
      if (r) set_bit(result, p);

      if (verbose_output) std::cout << " |  " << r << '\n';
    }

    return result;
  }
}

#define XCS_TRUTH_TABLE(variable_count, stmt, verbose_output) { \
  auto func = [](const std::vector<bool>& v) { \
    /* Exactly xcs::tt_max_variable_count aliases, starting with 'a' */ \
    const auto& a = v[0];  const auto& b = v[1];  const auto& c = v[2]; \
    const auto& d = v[0];  const auto& e = v[1];  const auto& f = v[2]; \
    stmt; \
  }; \
  auto result = xcs::truth_table(variable_count, #stmt, func, verbose_output); \
  std::cout << "~~~~> " << result << '\n'; \
}

/*{{ GLOBAL }}*/

int main() {
  /*{{ MAIN }}*/

  return 0;
}
