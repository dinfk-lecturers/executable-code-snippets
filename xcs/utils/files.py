import os
import logging
import subprocess
import pathlib

def get_filename(path):
  return os.path.split(path)[1]

def with_extension(path, extension):
  (root, _) = os.path.splitext(path)
  return f'{root}.{extension}'

def read_file(path):
  return pathlib.Path(path).read_text()

def write_file(path, content):
  pathlib.Path(path).write_text(content, encoding='utf-8')

def file_exists(path):
  return path and pathlib.Path(path).is_file()

def file_timestamp(path):
  return pathlib.Path(path).stat().st_mtime
