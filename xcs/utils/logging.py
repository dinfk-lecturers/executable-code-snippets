import logging
import argparse

# See https://docs.python.org/3.8/library/logging.html#logging-levels
LOG_LEVELS = {
  "DEBUG": logging.DEBUG,
  "INFO": logging.INFO,
  "WARNING": logging.WARNING,
  "ERROR": logging.ERROR,
  "OFF": logging.CRITICAL + 1
}

DEFAULT_LOGGING_LEVEL="INFO"
DEFAULT_LOG_FORMAT="[%(levelname)s] %(message)s"

def add_loglevel_argument(parser, default_level=DEFAULT_LOGGING_LEVEL):
  parser.add_argument(
    "--log-level",
    type=str,
    choices=("OFF", "DEBUG", "INFO", "WARNING", "ERROR"),
    default=default_level,
    help="Log level (default: {})".format(default_level))

def configure_level_and_format(level_string=DEFAULT_LOGGING_LEVEL, format=DEFAULT_LOG_FORMAT):
  # See https://docs.python.org/3.8/library/logging.html#logrecord-attributes
  logging.basicConfig(level=LOG_LEVELS[level_string], format=format)
