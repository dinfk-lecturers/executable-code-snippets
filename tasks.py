# -*- coding: utf-8 -*-

import os
import shutil
import sys
import datetime

from invoke import task
from invoke.util import cd

CONFIG = {
  'python': 'python.exe',
  'script': 'convert.py',
  'temp_path': 'tmp',
  'output_path': 'out',
  'port': 8000,
  'theme_path': 'theme',
}

@task
def clean(c):
  """Remove generated files"""
  for opt in ['temp_path', 'output_path']:
    dir = CONFIG[opt]
    if os.path.isdir(dir):
      shutil.rmtree(dir)

@task
def build(c, file_to_convert):
  """Build local version of site"""
  c.run(f"{CONFIG['python']} {CONFIG['script']} {file_to_convert}")

@task
def rebuild(c):
  """Clean, then build"""
  clean(c)
  build(c, file_to_convert)

@task
def serve(c):
  """Serve site at http://localhost:$PORT/ (default port is 8000)"""

  class AddressReuseTCPServer(RootedHTTPServer):
      allow_reuse_address = True

  server = AddressReuseTCPServer(
      CONFIG['output_path'],
      ('', CONFIG['port']),
      ComplexHTTPRequestHandler)

  sys.stderr.write('Serving on port {port} ...\n'.format(**CONFIG))
  server.serve_forever()

@task
def reserve(c, file_to_convert):
  """`build`, then `serve`"""
  build(c, file_to_convert)
  serve(c)

@task
def livereload(c, file_to_convert):
  """Automatically reload browser tab upon file modification."""
  from livereload import Server
  build(c, file_to_convert)
  server = Server()
  # Watch main script
  server.watch(CONFIG['script'], lambda: build(c, file_to_convert))
  # Watch file_to_convert
  server.watch(file_to_convert, lambda: build(c, file_to_convert))
  # Watch theme files
  server.watch(f"{CONFIG['theme_path']}/**/*", lambda: build(c, file_to_convert))
  # Serve output path on configured port
  server.serve(port=CONFIG['port'], root=CONFIG['output_path'])
