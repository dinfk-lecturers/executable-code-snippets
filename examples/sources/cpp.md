# Example: The Basics

This example illustrates the basics.

```c++
std::cout << "Lorem" << char(32) << "Ipsum";
```

<de>Welche Antwort beschreibt die Ausgabe am besten?</de>
<en>Which answer describes output best?</en>

1. <de>Lorem Ipsum</de> **[CORRECT]**
2. <de>Lorem-Ipsum</de>
3. <de>LoremIpsum</de> 
4. <de>Lorem&Ipsum</de>
5. <de>Lorem$Ipsum</de>

### Explanations & Comments

#### Regarding the Markdown source file

* It's all Markdown, and very few custom XML tags
* Order must be code, question, answers
* `<de>` tags are mandatory, `<en>` optional
* *Don't* place text between adjacent `...</de><en>...` tags

#### Regarding the generated HTML file

* The code yields three boxes in the output
    1. A syntax-highlighted text version of the code (ultimately redundant)
    2. The generated output: either compiler errors, or the code's output (for you to inspect)
    3. A syntax-highlighted image version of the code (for Moodle)
* Clicking the answer copies the answer and the syntax-highlighted image to clipboard, for pasting into Moodle text fields (switch them to raw HTML view, then paste)
* Each answer is analogously clickable
* Text outside of `de`/`en` tags is *not* copied (such as this whole explanation section, the **[CORRECT]** after one of the answers, or the text above the code boxes)



# Example: Types and Values

```c++
auto c = ((6 + 1) == 7) / 3 * 9;
XCS_INSPECT(c); /* --- no_show --- */
```

<de>Geben Sie Typ und Wert der Variable `c` an.</de>
<en>Provide type and value of variable `c`.</en>

**Moodle:** Two input fields

### Explanations & Comments

* The trailing `/* --- no_show --- */` comment prevents the line from appearing in the code image
* `XCS_INSPECT(exp)` outputs several facts about an expression
* The line "**Moodle:** Two ..." is merely a comment, and you need to manually create the appropriate input fields on Moodle.



# Example: Number Representation

<de>Konvertieren Sie die Zahlendarstellung wie angegeben.</de>
<en>Convert the number representations as instructed.</en>

1. <de>93 von dezimal nach hexadezimal</de>
   <en>from decimal to hexadecimal</en>
1. <de>11101 von binär nach dezimal</de>
   <en>from binary to decimal</en>
1. <de>11.625 von dezimal nach binär</de>
   <en>from decimal to binary</en>

```c++
// CODE NOT INTENDED FOR STUDENTS
std::cout << xcs::dec_to_base(93, 16) << '\n';
std::cout << xcs::dec_to_base(0b11101, 10) << '\n';
std::cout << xcs::dec_to_base(11.625, 2) << '\n';
```

**Moodle:** Two input fields

### Explanations & Comments

* Here, the code is below the answers because it should *not* be copied to the clipboard (this is basically a hack)
* `xcs::dec_to_base(value, base)` takes any valid C++ numeral (decimal, binary, hexadecimal, and octal representation) and converts it to the given base
* **Warning:** If `value` is a floating-point, `base` must be `2`!



# Example: Numerical Limits

```c++
// CODE NOT INTENDED FOR STUDENTS
XCS_INSPECT(7 - 8);
XCS_INSPECT(6 / 2u - 9);
int r1; unsigned int r2;
std::cout << "Overflow 7 - 8? " << __builtin_sub_overflow(7, 8, &r1) << '\n';
std::cout << "Overflow 6 / 2u - 9? " << __builtin_usub_overflow (6 / 2u, 9, &r2) << '\n';
```

<de>Welcher der folgende Ausdrücke resultiert in einem numerischen Unterlauf?</de>
<en>Which of the following expressions results in a numerical underflow?</en>

1. <de>`7 - 8`</de>
1. <de>`8 / 4.0 - 3`</de>
1. <de>`6 / 2u - 9`</de> **CORRECT**
1. <de>`7u * 2 - 21.0`</de>

### Explanations & Comments

The above snippet might not work on all compilers, but it's pretty cool :-)



# Example: Functions

```c++
void swap(int& x, int y) {
  int temp = x;
  x = y;
  y = temp;
}
/* --- main --- */
int a = 3; 
int b = 5;
swap(a, b);
std::cout << "a=" << a << " b=" << b; /* --- no_show --- */
```

<de>Welche Werte haben `a` und `b` am Ende des Codeschnippsels?</de>
<en>What are the final values of `a` and `b`?</en>

1. <de>3, 5</de>
2. <de>5, 3</de>
3. <de>3, 3</de>
4. <de>5, 5</de> **[CORRECT]**
5. <de>Unbekannt</de>

### Explanations & Comments

Comment `/* --- main --- */` separates function declarations (above) from the content of the `main` function (below).



# Example: No-Compile Marker

```c++
/* --- main --- */
unsigned int u = ...; // any value /* --- no_compile --- */
unsigned int u = -1; /* --- no_show --- */
assert(u < u + 1);
```

<de>Welche Aussage beschreibt den Code am besten?</de>
<en>Which statement describes the code best?</en>

1. <de>Assert hält immer</de><en>Assert always holds</en> 
1. <de>Assert hält nie</de><en>Assert never holds</en>
1. <de>Assert hält oder schlägt fehl, je nach Wert von `u`</de><en>Assert may or may not fail, depending on `u`'s value</en> **[CORRECT]**
1. <de>Undefined behaviour</de>

### Explanations & Comments

* Trailing comment `/* --- no_compile --- */` prevents the line from being compiled, but not from showing up in the syntax-highlighted code.
* The assigment `u = -1` was chosen to illustrate that the assertion can indeed fail (which of course doesn't mean that it will always fail)



# Example: No-Run Marker

```c++ no_run
// NONTERMINATING LOOP /* --- no_show --- */
int sum = 17;
int i = 1;

do {
  i += sum;
  sum = sum / 2;
} while (i > sum && sum >= 0);

std::cout << sum;
```

<de>Welche Aussage beschreibt die Ausgabe am besten?</de>
<en>Which statement describes the output best?</en>

1. <de>18</de> 
2. <de>17</de>
3. <de>8</de>
4. <de>Terminiert nie</de>
   <en>Never terminates</en> **[CORRECT]**

### Explanations & Comments

The code above indeed doesn't terminate. Hence, the snippet is compiled, but not executed (the code block is opened as ` ```c++ no_run`)



# Final Words

The `executable-code-snippets` tool works (and is basically awesome), but still offers plenty of room for improvements. Contributions are welcome.
