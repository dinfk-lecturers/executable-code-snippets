from setuptools import setup

setup(
  name='executable-code-snippets',
  version='0.0.1',
  description='Convert Markdown code snippets to HTML, and include compilation and execution output',
  url='https://gitlab.ethz.ch/dinfk-lecturers/executable-code-snippets.git',
  author='Malte Schwerhoff',
  author_email='malte.schwerhoff@inf.ethz.ch',
  license='Mozilla Public License Version 2.0',
  packages=[
    'xcs',
    'xcs.compilation',
    'xcs.utils',
  ],
  # package_data={
  #   "xcs": ["data/**"]
  # },
  include_package_data=True,
  python_requires='>=3.8',
  install_requires=[
    'Markdown>=3.3.3',
    'pymdown-extensions>=8.0.1',
    'Jinja2>=2.11.2',
    'Pygments>=2.7.2',
    'Pillow>=8.0.1',
  ],
  entry_points = {
    "console_scripts": [
      'xcs-convert = xcs.convert:main',
    ]
  },
  zip_safe=False)
