# Executable Code Snippets

A small tool for writing "Informatik I"-style theory exam questions for Moodle.

## Example

[Input file cpp.md](./examples/sources/cpp.md?plain=1) yields [this HTML output](https://people.inf.ethz.ch/scmalte/xcs/examples/generated/cpp.html) (also includes in this repository: `./examples/generated/cpp.html`).

## Requirements

* Python 3.8 (feel free to adapt to older and newer versions); all Python dependencies should be installed automatically. If you have multiple Python versions installed, make sure to use the right one.
* Command-line script `pygmentize` from https://pygments.org
* For C++ snippets: `gcc` compiler

## Installation

Install either directly from the Git repo, e.g.

    pip install --upgrade git+https://gitlab.ethz.ch/dinfk-lecturers/executable-code-snippets.git

or clone the sources, e.g. into `~/executable-code-snippets` and then install from there:

    pip install --upgrade -e ~/executable-code-snippets

The latter is useful for local development, since changes to the local clone immediately take effect.

During the installation, `xcs-convert` should have been added to the path, so that it can be run from anywhere:

    $ xcs-convert
    usage: xcs-convert [-h] file
    xcs-convert: error: the following arguments are required: file

## Usage

1. Copy `examples/sources/cpp.md`, includes in this repository, to, e.g. `~/xcs/cpp.md`
2. `cd ~/xcs` and then `xcs-convert cpp.md`
3. Open `out/cpp.html` in your browser

## License & Contributions

Sources are released under Mozilla Public License 2.0.

Contributions are welcome.
